 function [Markov]=computeMarkov(A,B,C,w,nx,ny,nu)
 
 q0=w(1)
 q=w(end)
 
 w=w(2:end-1)
 Aw = eye(nx,nx);
 if isempty(w)
           Aw = eye(nx,nx);
       else 
           for k=1:length(w)
               indx = w(k);
               
           Aw = A(:,:,indx+1)*Aw;
           
           end
       end
       Markov=C*Aw*B(:,:,q0+1);
  end     