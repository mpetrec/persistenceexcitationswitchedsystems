% LPV-SS affine realization 
% Example random LPV system with external inputs B
% Created 21 March 2019 MM
% For LPV S 2019

% # Asuming forward Innovation form
% 1. Estimate deterministic Ad(),Bd(),Cd(),Dd() using CRA, Cox et.al. IFAC'15
% 2. Estimate stochastic As(),Ks(),Cs using stochastic LPV-SSA Mejari, Petreczky CDC'19
     % using correlations (E{ys z^ys} = E{y z^y} - E{yd zyd})
     % WITHOUT using zero input excitation to get ys

% 3. Extended state space:
%    [Ad 0;0 As], [Bd;0], [Cd Cs] ; [0;Ks]

clc
clear all
close all

addpath(genpath('functions'));
randn('seed',1); rand('seed',1);
%load('generatedsystem.mat');
sys.nx = 3; sys.ny=1; sys.np=2;sys.nu=1;

A(:,:,1) = expm(0.4*[1 1 0;0 0 0;0 0 0]);
A(:,:,2) = expm(0.4*[0 0 0;0 1 1;0 1 1]);
A(:,:,3) = inv (A(:,:,1));
A(:,:,4) = inv (A(:,:,2));


B(:,:,1) = [1;1;1];
B(:,:,2) = [1;0;1];
B(:,:,3)=B(:,:,1);
B(:,:,4)=B(:,:,2);

%% See function fun_BtoK K = fun_BtoK(B,A,C,psig,niter)
% B = [0 0;0 0; 1 1];

%K(:,:,1) = [-0.0366;0;1];      % 0.4
%K(:,:,2) = [0;0.0146;1.1704];

C=[1 0 0];
D=0;

np=4
[nx]  = size(A,1);   % State dimension, External noise dimension, Scheduling signal dimension,
[ny,nu]     = size(D);  % Output dimension, Input dimension


 base.R{1,1} = [];    base.R{1,2} = 1; base.R{1,3} = 1;  % Beta
 base.R{2,1} = 1 ;    base.R{2,2} = 0; base.R{2,3} = 1;  % Beta
 base.R{3,1} = [1 0]; base.R{3,2} = 1; base.R{3,3} = 1;  % Beta
 
  base.O{1,1} = [];    base.O{1,2} = 0; base.O{1,3} = 1;  % alpha
  base.O{2,1} =  0;    base.O{2,2} = 0; base.O{2,3} = 1;  % alpha
  base.O{3,1} = [1 0]; base.O{3,2} = 0; base.O{3,3} = 1;  % alpha
 

u=[[1],[0],[0],[-1]]
p=[[0;1;0;0],[0;1;0;0];[0;1;0;0],[0;0;0,1]]



% Scheduling signal
%p(1,:) = randi(2,1,Ntot)-1;
%p(2,:) = 1-p(1,:);  %discrete scheduling
%v= 1*randn(ny,Ntot);          % noise signal
%v1 = 0.1*randn(ny,Ntot);
%v1= v;

%psig= ones(np,1);
% for i=1:np
%     psig(i,1) = mean(p(i,:)); % + (mean(p(i,:)))^2;
% end
 
% Input signal
%u = randn(nu,Ntot);

%xs = zeros(nx,Ntot);
%ys = zeros(ny,Ntot);
%ynfs = zeros(ny,Ntot);


Ntot=size(p,2);

x = zeros(nx,Ntot);
y = zeros(ny,Ntot);
ynf = zeros(ny,Ntot);

for k = 1:Ntot
    
    Atot = zeros(nx,nx);
%    Ktot = zeros(nx,ne);
    Btot = zeros(nx,nu);
    for j = 1:np
        Atot = Atot + A(:,:,j)*p(j,k);
 %       Ktot = Ktot + K(:,:,j)*p(j,k);
        Btot = Btot + B(:,:,j)*p(j,k);
    end
    
   
    
    x(:,k+1) = Atot*x(:,k)   + Btot*u(:,k); %+ Ktot*v(:,k);
    y(:,k)   = C*x(:,k) + D*u(:,k);  % eye(ny)*v(:,k);
    ynf(:,k) = C*x(:,k) + D*u(:,k);
   
end

%yt  = y(:,1:N);
%ut  = u(:,1:N);
%yts   = ys(:,1:N);
%ynf  = ynf(:,1:N);
%yval = y(:,N+1:Ntot);
%pt   = p(:,1:N);
%pval = p(:,N+1:Ntot);

%figure, plot(y(1,:)); title('Traning output');

%% SNR
%for i=1:ny
%    SNR_y(i,1) = 10*log10((ynf(i,:)*(ynf(i,:))'); %/(v(i,1:N)*(v(i,1:N))'))
%end

%% Deterministic identifcation (using Cox code ## to be done with own)
% base.R{i,1} = [1 2]; base.R{i,2} = 3; base.R{i,3} = 4;
% base.O{i,1} = [5];   base.O{i,2} = 6; base.O{i,3} = 7;  % alpha
%
% will select the 7th row and 4th column of (C_6 * A_2 * A_1 * A_5 * B_3)
%  ## select the 7th row and 4th column of (C_6 * A_5* A_2 * A_1  * B_3)

%return;


 
  [Hab_true]= true_det_hankel(C,A,B,base1,psig)
rank(Hab_true)


r=3;
alpha1(1).word = []; alpha1(2).word=1; alpha1(3).word = [2,1];
alpha1(1).index = 1; alpha1(2).index = 1; alpha1(3).index = 1;

beta1(1).word = []; beta1(2).word = 2; beta1(3).word = [2,1];
beta1(1).wordB = 2; beta1(2).wordB = 1; beta1(3).wordB = 2; % B matrix selection

beta1(1).index = 1; beta1(2).index =1; beta1(3).index = 1;


[Hab1,Habk1,Hak1,Hkb1,Adet,Bdet,Cdet,Ddet]= deter_lpv_realization_nice_Selection(yt,pt,ut,psig,alpha1,beta1,r);

% Verified with cox code
% % p1 = pt(2:end,:);    For cox code
% % tic 
% %         [Hab,Habk,Hak,Hkb] = hokalmanbasematrix(ut',(p1)',yt',base); % Estimate sub-Hankel matrices (eq.20 [1]) based upon the correlation anlysis (Section 3 [1])
% %         sysEst1 = hokalmanbaselpva(Hab,Habk,Hak,Hkb,np-1,nx);      % Realization of LPV model (Theorem 4 [1])
% %         sysEst1 = lpvaestdmat(sysEst1, ut',(p1)',yt');               % Estimate D_i matrices with correlation analysis (Section 3 [1])
% % toc
% % 
% %  Adet1 = sysEst1.A;
% %  Bdet1 = sysEst1.B;
% %  Cdet1 = sysEst1.C;
% %  Ddet1 = sysEst1.D;
% % %Adet = A; Bdet = B; Cdet= C; Ddet = D;
% %  %Cdet(:,:,1) = C;
% %  Cdet(:,:,2) = [0 0 0];
% % % Ddet(:,:,1) = D;
% %  Ddet(:,:,2) = 0;
 
 %% Simulate Deterministic System
 
xd = zeros(nx,N);
yd = zeros(ny,N);


for k = 1:N
    
    Atot = zeros(nx,nx);
    
    Btot = zeros(nx,nu);
    for j = 1:np
        Atot = Atot + Adet(:,:,j)*p(j,k);
        
        Btot = Btot + Bdet(:,:,j)*p(j,k);
    end
    
   
 
    
    xd(:,k+1) = Atot*xd(:,k)   + Btot*ut(:,k);
    yd(:,k)   = Cdet(:,:,1)*xd(:,k) + Ddet(:,:,1)*ut(:,k);
    %ynf(:,k) = C*x(:,k) + D*u(:,k);
   
end

figure, plot(yd); title('deterministic output');
 
%% Identification algorithm
L = nx;      % estimate of the max dimenstion of state space




%niter = 50;   % Number of iterations (N) Algorthm 2 Page 75 in paper Mihaly et.al TAC 2018 

% r,n selection
r = 3;n=1;
% Nice selection (3,3)-selection (\alpha,\beta)
%alpha(1).word = []; alpha(2).word=1; alpha(3).word = [2,1];
%alpha(1).index = 1; alpha(2).index = 1; alpha(3).index = 1;

%beta(1).word = []; beta(2).word = 2; beta(3).word = [2,1];
%beta(1).index = 1; beta(2).index =1; beta(3).index = 1;

%% nice selection from data

%yts= yt-yd;   % Stochastic output = observed - y_deterministic (simulated)
%[Aest,Kest,Qest,Pest,Cest,Best,H_red,H1,Test]= stoch_lpv_realization_nice_Selection(yts,pt,L,niter,psig,r,n,alpha,beta);
%[Aest,Kest,Qest,Pest,Cest,Best,H_red,H1,Test]= stoch_lpv_realization_nice_Selection_NEW(yt,yd,pt,L,niter,psig,r,n,alpha,beta);
%tend =toc
%% nice selection true matrices
%B= [0 0; 0 0;1 1];

%[Aest,Kest,Qest,Pest,Cest,Best,H_red,H1,Test,Z]= True_cov_stoch_lpv_realization_nice_Selection(yt,pt,L,niter,psig,r,n,alpha,beta,C,A,B);

%% SVD algo without nice selection
%[Aest,Kest,Qest,Pest,Cest,Best,H,Test]= stoch_lpv_realization_test(yt,pt,L,niter,psig);


%% Validation

% Fresh validation data set
%%xval = zeros(nx,Nval); 
%%vval = v(:,N+1:Ntot);
%%uval = u(:,N+1:Ntot);
%%yval = zeros(ny,Nval);
%%yval_nf = zeros(ny,Nval);
%%for k = 1:Nval
%%    
%%    Atot = zeros(nx,nx);
%%    Ktot = zeros(nx,ne);
%%    Btot = zeros(nx,nu);
%%    for j = 1:np
%%        Atot = Atot + A(:,:,j)*pval(j,k);
%%        Ktot = Ktot + K(:,:,j)*pval(j,k);
%%        Btot = Btot + B(:,:,j)*pval(j,k);
%%    end
%%    
%%    
%%    
%%     % Noise free validation data
%%        xval(:,k+1) = Atot*xval(:,k) + Btot*uval(:,k)+Ktot*vval(:,k);
%%        yval(:,k)   = C*xval(:,k) + D*uval(:,k)+ eye(ny)*vval(:,k);
%%        yval_nf(:,k) = C*xval(:,k)+ D*uval(:,k);
%%    
%%
%%end
%%
%%
%%%% One step ahead predictor
%%[nnx,~]= size(Aest(:,:,1));
%%xhat = zeros(2*nnx,Nval);
%%%xhat=xval;
%%yhat = zeros(ny,Nval);
%%
%%
%%for k = 1:Nval
%%    
%%    Atot1 = zeros(nnx,nnx);
%%    Ktot1 = zeros(nnx,ne);
%%    %Ktr = zeros(nnx,ne);
%%    
%%    Ad = zeros(nx,nx);
%%    Bd = zeros(nx,nu);
%%    %Cd = zeros(ny,nx);
%%    %Dd = zeros(ny,nu);
%%    
%%    for j = 1:np
%%        Atot1 = Atot1 + Aest(:,:,j)*pval(j,k);
%%        Ktot1 = Ktot1 + Kest(:,:,j)*pval(j,k);
%%        
%%        Ad = Ad + Adet(:,:,j)*pval(j,k);
%%        Bd = Bd + Bdet(:,:,j)*pval(j,k);
%%%        Cd = Cd + Cdet(:,:,j)*pval(j,k);
%%%        Dd = Dd + Ddet(:,:,j)*pval(j,k);
%%        
%%        %Ktr = Ktr + K(:,:,j)*pval(j,k);
%%    end
%%    
%%   Ahat = [Atot1 zeros(nx,nx); zeros(nx,nx) Ad];
%%   %Ahat = [Ad zeros(nx,nx); zeros(nx,nx) Ad];
%%   Bhat = [zeros(nx,nu);Bd];
%%   Chat = [Cest Cdet];
%%   %Chat = [C C];
%%   Khat = [Ktot1;zeros(nx,ny)];
%%   %Khat = [Ktr;zeros(nx,ny)];
%%   Dhat = Ddet;
%%    
%%    
%%    xhat(:,k+1) = Ahat*xhat(:,k) + Bhat*uval(:,k)+ Khat*(yval(:,k)- Chat*xhat(:,k)-Dhat*uval(:,k));
%%    
%%    yhat(:,k)   = Chat*xhat(:,k) + Dhat*uval(:,k);
%%    
%%
%%
%%end
%for k=2:Nval
%    yhat(:,k-1) = Cest*xhat(:,k); % one step ahead predicted output
%end
%figure,plot(yval_nf','r');hold on; plot(yhat'); title('Estimated vs True validatio output');
%% BFR, VAF and MSE

%%for i=1:ny
%%%    MSE_val(i,1) = mse(yval(i,:),yhat(i,:));
%%    
%%    ybar= mean(yval_nf(i,:));
%%    
%%    tmp1= (yval_nf(i,:)-yhat(i,:))*(yval_nf(i,:)-yhat(i,:))';
%%    tmp2= (yval_nf(i,:)-ybar)*(yval_nf(i,:)-ybar)';
%%    BFR_val(i,1)= max(0,1- sqrt(tmp1/tmp2)); 
%%    
%%end

%MSE_val
%BFR_val
%VAF_val= max(0,1- var(yval_nf-yhat)/var(yval_nf))

%% True system matrices

%[Htrue,Ptrue,Qtrue,Btrue, Ktrue] = true_stoch_lpv_matrices_new(yt,pt,x,v,L,psig,A,C,K,niter);
%% Stability
% Atil= zeros(size(kron(A(:,:,1),A(:,:,1))));
% for i=1:np
%   Atil =  Atil+ psig(i)*kron(A(:,:,i),A(:,:,i));
% end
% eig(Atil)
% 
% Atil_est= zeros(size(kron(Aest(:,:,1),Aest(:,:,1))));
% for i=1:np
%   Atil_est =  Atil_est+ psig(i)*kron(Aest(:,:,i),Aest(:,:,i));
% end
% eig(Atil_est)

%% PBSID
% mu_t = [(pt(1,:))' (pt(2,:))'];
% ut = randn(N,1); 
% % LPV identification
% fut= 8; past=8; % future past window size
% [S,x_h] = lordvarx(ut,yt,mu_t,fut,past,'tikh','gcv',[0 0 0]);
% x_h = lmodx(x_h,nx);
% 
% [Aid,Bid,Cid,Did,Kid] = lx2abcdk(x_h,ut,yt,mu_t,fut,past,[0 0 0]);
% %[Aid,Bid,Cid,Did,Kid] = lx2abcdk(x_h,ut,yt,mu_t,fut,past,[0 0 0],0,1);
% %[Aid1,Bid1,Cid1,Did1,Kid1] = lx2abcdk(x,u,y,mu,f,p,[0 0 0],0,1);
% figure, semilogy(S,'x');
% title('Singular values')
% 
% Apb(:,:,1) = Aid(1:nx,1:3);
% Apb(:,:,2) = Aid(1:nx,4:6);
% 
% Cpb = Cid(1:ny,1:3);
% Kpb(:,:,1) = Kid(:,1);
% Kpb(:,:,2) = Kid(:,2);
% 
% %%%%% Stability
% Atil_est_pb= zeros(size(kron(Apb(:,:,1),Apb(:,:,1))));
% for i=1:np
%   Atil_est_pb =  Atil_est_pb+ psig(i)*kron(Apb(:,:,i),Apb(:,:,i));
% end
% eig(Atil_est_pb)
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % validation of PBSID estimates with one step ahead predictor
% [nnx,~]= size(Apb(:,:,1));
% xhat = zeros(nnx,Nval);
% %xhat=xval;
% yhat_pb = zeros(ny,Nval);
% 
% 
% for k = 1:Nval
%     
%     Atot1 = zeros(nnx,nnx);
%     Ktot1 = zeros(nnx,ne);
%     for j = 1:np
%         Atot1 = Atot1 + Apb(:,:,j)*pval(j,k);
%         Ktot1 = Ktot1 + Kpb(:,:,j)*pval(j,k);
%     end
%     
%    
%     xhat(:,k+1) = Atot1*xhat(:,k) + Ktot1*(yval(:,k)- Cpb*xhat(:,k));
%     
%     yhat_pb(:,k)   = Cpb*xhat(:,k);
%     
% 
% 
% end
% %for k=2:Nval
% %    yhat(:,k-1) = Cest*xhat(:,k); % one step ahead predicted output
% %end
% figure,plot(yval_nf','r');hold on; plot(yhat_pb'); title('Estimated vs True validation output PBSID');
% %% BFR, VAF and MSE
% 
% for i=1:ny
% %    MSE_val(i,1) = mse(yval(i,:),yhat(i,:));
%     
%     ybar= mean(yval_nf(i,:));
%     
%     tmp1= (yval_nf(i,:)-yhat_pb(i,:))*(yval_nf(i,:)-yhat_pb(i,:))';
%     tmp2= (yval_nf(i,:)-ybar)*(yval_nf(i,:)-ybar)';
%     BFR_val_PBSID(i,1)= max(0,1- sqrt(tmp1/tmp2)); 
%     
%     VAF_val_PBSID(i,1) = max(0,1- var(yval_nf(i,:)-yhat_pb(i,:))/var(yval_nf(i,:)))
% end
% 
% %MSE_val
% BFR_val_PBSID

%% Markov parameters true vs estimated
 
 
 C_algo = Cdet;
 B_algo(:,:,1) = Bdet(:,:,1);
 B_algo(:,:,2) = Bdet(:,:,2);
 
 A_algo(:,:,1)= Adet(:,:,1);
 A_algo(:,:,2)= Adet(:,:,2);
 
 
 [C*A(:,:,1)*B(:,:,1), C_algo*A_algo(:,:,1)*B_algo(:,:,1);
  C*A(:,:,1)*A(:,:,1)*B(:,:,1), C_algo*A_algo(:,:,1)*A_algo(:,:,1)*B_algo(:,:,1); 
  C*A(:,:,2)*B(:,:,2), C_algo*A_algo(:,:,2)*B_algo(:,:,2);
  C*(A(:,:,1))*(A(:,:,2))*B(:,:,2),C_algo*A_algo(:,:,1)*A_algo(:,:,2)*B_algo(:,:,2);
  C*A(:,:,2)*B(:,:,1), C_algo*A_algo(:,:,2)*B_algo(:,:,1);
  C*A(:,:,1)*B(:,:,2), C_algo*A_algo(:,:,1)*B_algo(:,:,2);
  C*(A(:,:,1))^3*B(:,:,1) C_algo*(A_algo(:,:,1))^3*B_algo(:,:,1)
 ] 
 

