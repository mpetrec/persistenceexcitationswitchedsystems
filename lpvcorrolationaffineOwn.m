function R = lpvcorrolationaffineOwn(z, p, u)
%LPVCORROLATIONAFFINE LPV correlation structure for affine estimation
%
%  Extimates the N-dimensional cross-correlation (Definition 2, [1])
%  between the given signals. 
%
% R = lpvcorrolationaffine(z, p, u)
%
% z  The signals [y u p]. p can have multiple columns.
% p  Shift parameters of p. This is a matrix with p(1,:) the indexes of the
%    used signal of p in z and p(2,:) the shift of them.
% u  Shift parameter of u
%
% R  Cross-correlation between the signals
%
%
% [1] P.B. Cox, R. T�th, and M. Petreczky, 'Estimation of LPV-SS Models
%     with Static Dependency using Correlation Analysis', Proc. of the 1st
%     IFAC Workshop on Linear Parameter Varying Systems, p. 91-96 Grenoble,
%     France, October 7-9, 2015
%
% See also HOKALMANBASEMATRIX, LPVAESTDMAT

% 08/01/2016
% v 1.1
%
% (c) Copymiddle 2016 Pepijn Cox (p.b.cox@tue.nl), Eindhoven University of
% Technology 
%
% Changelog:
% v1.1 - Include more comments
%


        % Regular corrolation matrix without using p
    if isempty(p)
        Rt = covf([z(:,1), z(:,2)],u+1);
    else
        if(max(p(2,:)) > u)
        	error('not posible, p u');
        elseif(min(p)<0)
            error('not posible, p');
        end
        
        % Correlation matrix with p
        
        yt = z(u+1:end,1);
        
        for i = 1:length(p(1,:))
            yt = yt.*z(u-p(2,i)+1:end-p(2,i),p(1,i)+2);
        end
        
            % Time shifting has been done already. Therefore only shift 0
            % is necessary
        Rt = covf([yt, z(1:end-u,2)],1);
    end
    
   R = Rt(2,end);

end