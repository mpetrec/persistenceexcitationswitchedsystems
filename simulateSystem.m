function [xf,x,y]=simulateSystem(A,B,C,np,nx,nu,ny,u,sequence,x0)

  x=zeros(nx,size(u,2)+1);
  y=zeros(ny,size(u,2));
  x(:,1)=x0;

  for t=1:size(u,2)
	x(:,t+1)=A(:,:,sequence(t)+1)*x(:,t)+B(:,:,sequence(t)+1)*u(:,t);
	y(:,t)=C*x(:,t);
  end 
 

  xf=x(:,end);
end

