%Algorithm 2 of the paper
function [Ad,Bd,Cd]=det_realSelection(Hab,Habk,Hak,Hkb,np,nu,ny)
r=size(Hab,1)
d=np
Ad= zeros(r,r,d); Bd= zeros(r,nu,d); Cd= zeros(ny,r,d); 

%   [U,S, V] = svd(Hab,0);         % Full rank decomposition

%    U = U(:,1:r);                % Select only the nx basis of the Hankel matrix
%    V = V(:,1:r);
%    S = S(1:r,1:r);

    % inverse reachability and observablity matrix.
 %   Oinv = (S^(-1/2))*U';
 %   Rinv = V * (S^(-1/2));
   
   
    Oinv = inv(Hab)
    Rinv = eye(size(Oinv))
    for i = 1:d
        Ad(:,:,i) = Oinv* Habk(:,:,i) * Rinv;

        % Discrete time
        eigtmp = eig(Ad(:,:,i));
        if(~isempty(find(  real(eigtmp).^2 + imag(eigtmp).^2 > 1  )))
            warning(['The matrix A',sprintf('%.0f',i),' is unstable. Poles outside unit disc']);
        end

        Bd(:,:,i) = Oinv* Hak(:,:,i);
        Cd(:,:,i) = Hkb(:,:,i)*Rinv
        

    end
    %Cd = Hkb*Rinv;

end
