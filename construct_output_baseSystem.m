
%Implementation of Algorithm 3 of the accompanying paper
function [u,p,t, markov_words]=construct_output_baseSystem(A,B,C,np,nu,ny,base,contr_base)
	t=[];
	u=[]
	p=[]
  markov_words={}
	for i=1:size(base.O,1)
    for q=1:np
        markov_words=[markov_words;[q-1,base.O{i,1},base.O{i,2}]];
    end
		for j=1:size(base.R,1)			
        markov_words=[markov_words;[base.R{j,2},base.R{j,1},base.O{i,1},base.O{i,2}]];			
        for q=1:np
          markov_words=[markov_words;[base.R{j,2},base.R{j,1},q-1,base.O{i,1},base.O{i,2}]];
        end
    end    
  end
  for j=1:size(base.R,1)			
       for q=1:np
         markov_words=[markov_words;[base.R{j,2},base.R{j,1},q-1]];
       end 
  end      
   
  for j=1:size(markov_words,1)
      for index=1:nu
			  [u1,p1,t_index]=construct_input_sequenceSystem(markov_words{j,1},index,np,nu,ny,A,B,C,contr_base);
			  t=[t;size(u,2)+t_index];
			  u=[u,u1];
			  p=[p,p1];
      end 
   end     
		
	end	
