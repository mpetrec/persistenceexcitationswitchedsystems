
clc
clear all
close all

addpath(genpath('functions'));
randn('seed',1); rand('seed',1);
%load('generatedsystem.mat');
%sys.nx = 3; sys.ny=1; sys.np=2;sys.nu=1;

A(:,:,1) = expm(0.4*([1 1 0;0 0 0;0 0 0]-2*eye(3)));
A(:,:,2) = expm(0.4*([0 0 0;0 1 1;0 1 1]-2*eye(3)));
%A(:,:,3) = inv (A(:,:,1));
%A(:,:,4) = inv (A(:,:,2));


B(:,:,1) = [1;1;1];
B(:,:,2) = [1;0;1];
%B(:,:,3)=B(:,:,1);
%B(:,:,4)=B(:,:,2);

%% See function fun_BtoK K = fun_BtoK(B,A,C,psig,niter)
% B = [0 0;0 0; 1 1];

%K(:,:,1) = [-0.0366;0;1];      % 0.4
%K(:,:,2) = [0;0.0146;1.1704];

C=[1 0 0];
D=0;

np=2
[nx]  = size(A,1);   % State dimension, External noise dimension, Scheduling signal dimension,
[ny,nu]     = size(D);  % Output dimension, Input dimension


  base.R{1,1} = [];    base.R{1,2} = 1; base.R{1,3} = 1;  % Beta
  base.R{2,1} = 1 ;    base.R{2,2} = 0; base.R{2,3} = 1;  % Beta
  base.R{3,1} = [1 0]; base.R{3,2} = 1; base.R{3,3} = 1;  % Beta
 
  base.O{1,1} = [];    base.O{1,2} = 0; base.O{1,3} = 1;  % alpha
  base.O{2,1} =  0;    base.O{2,2} = 0; base.O{2,3} = 1;  % alpha
  base.O{3,1} = [1 0]; base.O{3,2} = 0; base.O{3,3} = 1;  % alpha
 


  base1.R={};
  base1.O={}
  for i=1:np
	for j=1:nu
		base1.R=[base1.R; {[],i-1,j}];
        end
	for r=1:ny
		base1.O=[base1.O; {[],i-1,j}];
        end
  end	 
   
  base2=base1

  for k=1:nx
      for q=1:np
	      for i=1:size(base1.R,1)
		  base1.R{i,1}=[base1.R{i,1},q-1];      
              end
	      for i=1:size(base1.O,1)
		   base1.O{i,1}=[base1.O{i,1},q-1];
	      end
      end
      base1.R=[base2.R;base1.R]; 
      base1.O=[base2.O;base1.O];
 end
		
  
base1



%Construct Hankel-matrix
base3=base


dict_inv{1}=3
dict_inv{2}=4
dict_inv{3}=1
dict_inv{4}=2

[u,p,tt,markov_words]=construct_output_baseSystem(A,B,C,np,nu,ny,base3,base3.R)





%u=[[1],[0],[-1]]
%p=[[0;1;0;0],[0;1;0;0],[0;0;0;1]]





% Scheduling signal
%p(1,:) = randi(2,1,Ntot)-1;
%p(2,:) = 1-p(1,:);  %discrete scheduling
%v= 1*randn(ny,Ntot);          % noise signal
%v1 = 0.1*randn(ny,Ntot);
%v1= v;

%psig= ones(np,1);
% for i=1:np
%     psig(i,1) = mean(p(i,:)); % + (mean(p(i,:)))^2;
% end
 
% Input signal
%u = randn(nu,Ntot);

%xs = zeros(nx,Ntot);
%ys = zeros(ny,Ntot);
%ynfs = zeros(ny,Ntot);


Ntot=size(p,2);

x = zeros(nx,Ntot);
y = zeros(ny,Ntot);
ynf = zeros(ny,Ntot);

for k = 1:Ntot
    
    Atot = zeros(nx,nx);
%    Ktot = zeros(nx,ne);
    Btot = zeros(nx,nu);
    for j = 1:np
        Atot = Atot + A(:,:,j)*p(j,k);
 %       Ktot = Ktot + K(:,:,j)*p(j,k);
        Btot = Btot + B(:,:,j)*p(j,k);
    end
    
   
    
    x(:,k+1) = Atot*x(:,k)   + Btot*u(:,k); %+ Ktot*v(:,k);
    y(:,k)   = C*x(:,k) + D*u(:,k);  % eye(ny)*v(:,k);
    ynf(:,k) = C*x(:,k) + D*u(:,k);
   
end

psig=ones(1,np);

[Hab_true]= true_det_hankel(C,A,B,base3,psig)

M_est={}
M_true={}
M_diff={}
count=1;
for i=1:size(markov_words,1)
    Mark_local=[]
    for index=1:nu
		  Mark_local=[Mark_local,y(:,tt(count))];
    end
    M_est=[M_est; {markov_words{i,1}, Mark_local}];
    M_true=[M_true; {markov_words{i,1}, computeMarkov(A,B,C,markov_words{i,1},nx,ny,nu)}];
    M_diff=[M_diff; {markov_words{i,1}, M_est{count,2}-M_true{count,2}}];
		count = count + 1    
end



%yt  = y(:,1:N);
%ut  = u(:,1:N);
%yts   = ys(:,1:N);
%ynf  = ynf(:,1:N);
%yval = y(:,N+1:Ntot);
%pt   = p(:,1:N);
%pval = p(:,N+1:Ntot);

%figure, plot(y(1,:)); title('Traning output');

%% SNR
%for i=1:ny
%    SNR_y(i,1) = 10*log10((ynf(i,:)*(ynf(i,:))'); %/(v(i,1:N)*(v(i,1:N))'))
%end

%% Deterministic identifcation (using Cox code ## to be done with own)
% base.R{i,1} = [1 2]; base.R{i,2} = 3; base.R{i,3} = 4;
% base.O{i,1} = [5];   base.O{i,2} = 6; base.O{i,3} = 7;  % alpha
%
% will select the 7th row and 4th column of (C_6 * A_2 * A_1 * A_5 * B_3)
%  ## select the 7th row and 4th column of (C_6 * A_5* A_2 * A_1  * B_3)

%return;
