%Algorithm 4 of the paper
function [u,p,t,markov_words]=construct_output_base(base,dict_inv,np,nu,ny)
	t=[];
	u=[]
	p=[]
  markov_words={}
	for i=1:size(base.O,1)
    for q=1:np
        markov_words=[markov_words;[q-1,base.O{i,1},base.O{i,2}]];
    end
		for j=1:size(base.R,1)			
        markov_words=[markov_words;[base.R{j,2},base.R{j,1},base.O{i,1},base.O{i,2}]];			
        for q=1:np
          markov_words=[markov_words;[base.R{j,2},base.R{j,1},q-1,base.O{i,1},base.O{i,2}]];
        end
    end    
  end
  for j=1:size(base.R,1)			
       for q=1:np
         markov_words=[markov_words;[base.R{j,2},base.R{j,1},q-1]];
       end 
  end      
   
  for j=1:size(markov_words,1)
      for index=1:nu
			  [u1,p1,t_index]=construct_input_sequence(markov_words{j,1},index,dict_inv,np,nu,ny);
        %construct_input_sequence(markov_words{j,1},index,np,nu,ny,A,B,C,contr_base);
			  t=[t;size(u,2)+t_index];
			  u=[u,u1];
			  p=[p,p1];
      end 
   end     
		
	end	
	%for i=1:size(base.O,1)
	%	for j=1:size(base.R,1)
	%		word=[base.R{j,2},base.R{j,1},base.O{i,1},base.O{i,2}];
	%		word
	%		[u1,p1,t_index]=construct_input_sequence(word,base.R{j,3},dict_inv,np,nu,ny);
	%		t=[t;[size(u,2)+t_index,base.O{i,3}]];
	%		u=[u,u1];
	%		p=[p,p1];
	%	end
	%end	
