% PBSID ALGO TOOLBOX

% CDC 19 paper example with external inputs
close all; clear all; clc;
randn('seed',1); rand('seed',1);

n = 3;  % The order of the system
m = 2;  % The number of scheduling parameters
r = 1;  % The number of inputs
l = 1;  % The number of outputs

% System matrices
A1 = 0.4*[1 1 0;0 0 0;0 0 0];
A2 = 0.4*[0 0 0;0 1 1;0 1 1];

%A1 = (A1t + A2t)/2;
%A2 = (A1t - A2t)/2;

B1 = [1;1;1];
B2 = [1;0;1];


K1 = [-0.0366;0;1];
K2 = [0;0.0146;1.1704];

%K1= (K1t+K2t)/2;
%K2= (K1t-K2t)/2;

C=[1 0 0];
D = 1;

Alpv = [A1 A2];
Blpv = [B1 B2];
Clpv = [C zeros(l,(m-1)*n)]; %[C zeros(l,3*n)];
Dlpv = [D 0];%[D zeros(l,3*r)];
Klpv = [K1 K2];

% Measured data from the scheduling parameters
N = 1000000;  % number of data points
t = (0:1:(N-1))';
%rho = rand(N,1);
%mu = [ones(N,1) rho 0.5.*sin((2*pi/100).*t).*rho 0.5.*cos((2*pi/100).*t).*rho];

mu = [ones(N,1) 1*(3*rand(N,1)-1.5)];% (1*rand(N,1)-0.5) (1*rand(N,1)-0.5)];
% Measured input data

%[b,a] = butter(2,0.2);
%u = [filter([0.75 1.05 0.15],1,eta) + filter(b,a,nu) xi];
u = 3*rand(N,r)-1.5;

% Simulation of the system without noise
M = idafflpv(Alpv,Blpv,Clpv,Dlpv,Klpv,[],1);
y0 = sim(M,u,t,mu(:,2:end));

% Simulation of the system with noise
%e = 0.5.*randn(N,l);
e = 1.*randn(N,l);
y = sim(M,u,t,mu(:,2:end),e);
disp('Signal to noise ratio (SNR) (open-loop)')
snr(y,y0)

% Defining a number of constants
p = 5;     % past window size
f = 5;     % future window size

% LPV identification
[S,x] = lordvarx(u,y,mu,f,p,'tikh','gcv',[0 0 0]);
x = lmodx(x,n);
[Aid,Bid,Cid,Did,Kid] = lx2abcdk(x,u,y,mu,f,p,[0 0 0]);
%[Aid1,Bid1,Cid1,Did1,Kid1] = lx2abcdk(x,u,y,mu,f,p,[0 0 0],0,1);
figure, semilogy(S,'x');
title('Singular values')

Mi = idafflpv(Aid,Bid,Cid,Did,Kid,[],1);
[e,x0] = pe(Mi,u,y,t,mu(:,2:end),'CD');
Mi.x0 = x0;
Mi.NoiseVariance = cov(e);
Mp = pem(Mi,u,y,t,mu(:,2:end),'CD');

%% Validation data from the scheduling parameters


mu = [ones(N,1) (3*rand(N,1)-1.5)];% (1*rand(N,1)-0.5) (1*rand(N,1)-0.5)];
% Validation data
%nu = randn(N,1);
%eta = randn(N,1);
%xi = randn(N,1);
%[b,a] = butter(2,0.2);
%u = [filter([0.75 1.05 0.15],1,eta) + filter(b,a,nu) xi];
u = 3*rand(N,r)-1.5;

% Simulation of the system without noise
y0 = sim(M,u,t,mu(:,2:end));

% Simulation of identified LPV system
yid = sim(Mi,u,t,mu(:,2:end));
disp('VAF of identified LPV system')
vaf(y0,yid)

BFR_val = max(0,1- norm(y0-yid)/norm(y0-mean(y0)))

% Simulation of optimized LPV system
yid = sim(Mp,u,t,mu(:,2:end));
disp('VAF of optimized LPV system')
vaf(y0,yid)



%%
