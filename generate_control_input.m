%generate w(x), eq. 26 of the paper
function [u,sequence,ctr_matrix]=generate_control_input(xf,A,B,C,nx,np,nu,ny,base_cont)
	sequence = generate_universal_sequenceSystem(A,B,np,nx,nu,ny,base_cont);
	ctr_matrix=B(:,:,sequence(1)+1);
	xfin1=A(:,:,sequence(1)+1)*xf;

	for j=2:size(sequence,2)
		ctr_matrix=[A(:,:,sequence(j)+1)*ctr_matrix,B(:,:,sequence(j)+1)];
		xfin1=A(:,:,sequence(j)+1)*xfin1;
	end

	
	
	rank(ctr_matrix)

  	u=-lscov(ctr_matrix,xfin1);

	disp('Error')
	norm(ctr_matrix*u+xfin1)
	
	u=reshape(u,nu,[]);
	[xf2,x,y]=simulateSystem(A,B,C,np,nx,nu,ny,u,sequence,xf)
end
	
