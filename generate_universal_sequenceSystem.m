%eq. 22 of the paper
function [sequence] = generate_universal_sequenceSystem(A,B,np,nx,nu,ny,base_contr)

  sequence = [base_contr{1,2},base_contr{1,1}] 
  for i=2:size(base_contr,1)
	sequence=[sequence, repmat(flip(sequence),1,nx-1), base_contr{i,2},base_contr{i,1}]
  end



end
