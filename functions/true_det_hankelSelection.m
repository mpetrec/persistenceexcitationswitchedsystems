function [Hab_true,Habk_true,Hka_true,Hkb_true] = true_det_hankelSelection(C,A,B,base,psig)
% Calculates True hankel matrix

n = size(base.R,1);
d = length(psig);
np=size(A,3);
[nx,~]= size(A(:,:,1));
Hab_true = zeros(n,n);
Habk_true=zeros(n,n,np);
Hkb_true=zeros(size(C,1),n,np);
Hka_true=zeros(n,size(B,2),np);
for i = 1:size(base.O,1)
        for j = 1:size(base.R,1)
            
            w = [base.R{j,1}, base.O{i,1}];
            Aw = eye(nx,nx); pw=1;
       if isempty(w)
           Aw = eye(nx,nx);
       else 
           for k=1:length(w)
               indx = w(k);
               
           Aw = A(:,:,indx+1)*Aw;
           
           end
       end

%Hab_true(i,j) = (C(:,:,base.O{j,2}+1)*Aw*B(:,:,base.R{j,2}+1));
    
    Hab_true(i,j) = (C(base.O{j,3},:)*Aw*B(:,base.R{j,3},base.R{j,2}+1));
   end
end


for q=1:np
    for i = 1:size(base.O,1)
        for j = 1:size(base.R,1)
            
            w = [base.R{j,1}, q-1,base.O{i,1}];
            Aw = eye(nx,nx); pw=1;
         if isempty(w)
           Aw = eye(nx,nx);
         else 
           for k=1:length(w)
               indx = w(k);
               
           Aw = A(:,:,indx+1)*Aw;
           
           end
          end
           Habk_true(i,j,q)=C(base.O{j,3},:)*Aw*B(:,base.R{j,3},base.R{j,2}+1);
        end
    end
 end
  
  for q=1:np
    for i = 1:size(base.O,1)
      w = [base.O{i,1}];
            Aw = eye(nx,nx); pw=1;
         if isempty(w)
           Aw = eye(nx,nx);
         else 
           for k=1:length(w)
               indx = w(k);
               
           Aw = A(:,:,indx+1)*Aw;
           
           end
          end
      
       Hka_true(i,:,q)=C(base.O{i,3},:)*Aw*B(:,:,q);
       
    end
    for j=1:size(base.R,1)
        w = [base.R{j,1}];
            Aw = eye(nx,nx); pw=1;
         if isempty(w)
           Aw = eye(nx,nx);
         else 
           for k=1:length(w)
               indx = w(k);
               
           Aw = A(:,:,indx+1)*Aw;
           
           end
          end
      
       Hkb_true(:,j,q)=C*Aw*B(:,base.R{j,3},base.R{j,2}+1);
    end
  end
            
            
      
end





