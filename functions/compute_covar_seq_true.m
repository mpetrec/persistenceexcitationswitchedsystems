function Psi = compute_covar_seq_true(w,psig,C,A,B)


Psi = [];

if w==0
    w=[];
end


[nx,~]= size(A(:,:,1));


Aw = eye(nx,nx); pw=1;
       if isempty(w)
           Aw = eye(nx,nx);
       else 
           for k=1:length(w)
               indx = w(k);
               
           Aw = A(:,:,indx)*Aw;
           pw = pw*psig(indx);
           end
       end

Psi = (C*Aw*B)*sqrt(pw);

%Psi = (C*Aw*B);

end

