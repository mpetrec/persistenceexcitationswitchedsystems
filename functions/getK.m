% get K from B
clc
clear all

A(:,:,1) = [-1.3, -0.6325, -0.1115, 0.0596;
            1, 0, 0, 0;
            0, 1, 0, 0;
            0, 0, 1, 0];


A(:,:,2) = [-0.51, -0.1075, -0.007275, -0.0000625;
             1, 0, 0, 0;
             0, 1, 0, 0;
             0, 0, 1, 0];
         

A(:,:,3) = diag([0.2,0.4,0,0]);
A(:,:,4) = diag([0,0,0.3,0.3]);

C=[1 0 0 0;0 1 0 0];


B(:,:,1)  = [0 1;1 0;1 0;0 1];
B(:,:,2)  = [0 0;0 0;0 0;0.3 0.3];
B(:,:,3)  = [0 0;0 0;0 0;0.3 0.3];
B(:,:,4)  = [0 0;0 0;0 0;0.3 0.3];


N    = 500000;     % Traing data
Nval = 20000;      % Validation data
Ntot = N+Nval; 

% Scheduling signal
p(1,:) = 0.2*ones(1,Ntot);
p(2,:) = 1*rand(1,Ntot)-0.5;
p(3,:) = 2*rand(1,Ntot)-1;
p(4,:) = 1*rand(1,Ntot)-0.5;


np = 4;  niter =100;
psig= ones(np,1);
 for i=1:np
     psig(i,1) = var(p(i,:)) + (mean(p(i,:)))^2;
 end
K = fun_BtoK(B,A,C,psig,niter);
