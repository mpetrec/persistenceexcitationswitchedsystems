function [Htrue,H_red_true,Ptrue,Qtrue,B,Knew,Tt] = true_stoch_lpv_matrices_new(ny,d,nx,psig,A,C,K,niter,alpha,beta,r)


%% Consrtuction of True covariances 

% From words of length at most L (2L-1)
%[d,M]= size(pt);
%[ny,~]= size(yt);
%[nx,~]= size(x);
L = nx;
W = cell((L-1)+1+1,1);  % Collection of words of lenth at most (L)
W{1}= [];
ind=1;
WW{ind} = [];
ind= ind+1;
for l=1:(L-1)+1
    W{l+1} = permn((1:d)',l);
    
    for j=1:size(W{l+1})
        WW{ind} = W{l+1}(j,:);
        ind= ind+1;
    end
end

%% True covarances
B=[];
for sig=1:d
%xtemp = x(:,1:M).*(pt(sig,1:M));
%vtemp = v(:,1:M).*(pt(sig,1:M));
%P(:,:,sig) = cov(xtemp');
Q(:,:,sig) = psig(sig)*eye(ny);

%Btrue(:,:,sig) = (1/sqrt(psig(sig)))*(A(:,:,sig)*P(:,:,sig)*C' + ...
 %                K(:,:,sig)*Q(:,:,sig)) ;
%B = [B Btrue(:,:,sig)];

%Tt(:,:,sig) = (1/psig(sig))*(C*P(:,:,sig)*C' + Q(:,:,sig));

end


nm=nx;
% initialize P,Q,K
 for sig=1:d
     P(:,:,sig) = zeros(nm,nm);
%     Tsig(:,:,sig) =  compute_T(yt,pt,sig,psig,M);
%     Q(:,:,sig) = psig(sig)*Tsig(:,:,sig) ;
%     K(:,:,sig) = sqrt(psig(sig))*Bsig(:,:,sig)*inv(Q(:,:,sig));
 end

Pold = P; Pnew = P;
 for i=1:niter
% 
%     
     for sig=1:d
%          
         temp = zeros(nm,nm);
      for sig1=1:d
%             % if (sig1,sig) in L (language)
      %        P(:,:,sig) = P(:,:,sig)+ ...
      %                    A(:,:,sig1)*P(:,:,sig1)*(A(:,:,sig1))'+...
       %                   K(:,:,sig1)*Q(:,:,sig1)*(K(:,:,sig1))';
%            
           temp = temp +   A(:,:,sig1)*Pold(:,:,sig1)*(A(:,:,sig1))'+...
                           K(:,:,sig1)*Q(:,:,sig1)*(K(:,:,sig1))';
%             
     end
        % P(:,:,sig)= psig(sig)*P(:,:,sig);
         Pnew(:,:,sig) =  psig(sig)*temp ;
%         Q(:,:,sig)= psig(sig)*Tsig(:,:,sig)- Cest*P(:,:,sig)*Cest';
%         K(:,:,sig)= (sqrt(psig(sig))*Bsig(:,:,sig)-Asig(:,:,sig)* P(:,:,sig)*Cest')*inv(Q(:,:,sig));
 %     norm(Pnew(:,:,sig)-Pold(:,:,sig))
     end
     
     Pold = Pnew;
     
    
     
 end
 
 Ptrue= Pold;
 
 %% Btrue
for sig=1:d
    
  Btrue(:,:,sig) = (1/sqrt(psig(sig)))*(A(:,:,sig)*Ptrue(:,:,sig)*C' + ...
                 K(:,:,sig)*Q(:,:,sig)) ;
B = [B Btrue(:,:,sig)];

Tt(:,:,sig) = (1/psig(sig))*(C*Ptrue(:,:,sig)*C' + Q(:,:,sig));
end
 
 %% True Hankel Matrix

%[d,M]= size(pt);

MLr =  (d^((L-1)+1)-1)/(d-1);
MLc =  (d^((L-1)+2)-1)/(d-1);

Psi_r=[]; Psi_c=[];
for i=1:MLr
    Psi_r=[];
    for j=1:MLc
       w =  [WW{j} WW{i}];
        Aw = eye(nx,nx); pw=1;
       if isempty(w)
           Aw = eye(nx,nx);
       else 
           for k=1:length(w)
               indx = w(k);
           Aw = A(:,:,indx)*Aw;
           pw = pw*psig(indx);
           end
       end
       
       
       Psi_temp = (C*Aw*B)*sqrt(pw);
       Psi_r= [Psi_r Psi_temp];
        
    end
    Psi_c = [Psi_c;Psi_r];
end

Htrue = Psi_c;

%% H_red based on selections
H_red = zeros(nx,nx);
for j=1:r
    for i=1:r
        w = [beta(j).word alpha(i).word];
        
        Psi_t = compute_covar_seq_true(w,psig,C,A,B);
        
        H_red(i,j) = Psi_t(alpha(i).index,beta(j).index);
    end
end
H_red_true = H_red;

%Ptrue = P;
Qtrue = Q;
%%
% Asig(:,:,1) = [0 0.0734; -6.5229 -0.4997];
% Asig(:,:,2) = [-0.00212 0; -0.0138 0.5196];
% 
% Bsig(:,:,1)= [-0.7221; -9.6277];
% Bsig(:,:,2)= [0; 0];
% 
% Cest = C;
% 
% [nm,~] = size(Asig(:,:,1)); 
% for i=1:niter
%     
%     % initialize P,Q,K
% % for sig=1:d
% %     P(:,:,sig) = zeros(nm,nm);
% %     Tsig(:,:,sig) =  compute_T(yt,pt,sig,psig,M);
% %     Q(:,:,sig) = psig(sig)*Tsig(:,:,sig) ;
% %     K1(:,:,sig) = sqrt(psig(sig))*Bsig(:,:,sig)*inv(Q(:,:,sig));
% % end
% % 
% %     
% %     for sig=1:d
% %         
% %         for sig1=1:d
% %             % if (sig1,sig) in L (language)
% %             P(:,:,sig) = P(:,:,sig)+ ...
% %                          Asig(:,:,sig1)*P(:,:,sig1)*(Asig(:,:,sig1))'+...
% %                          K1(:,:,sig1)*Q(:,:,sig1)*(K1(:,:,sig1))';
% %            
% %             
% %         end
% %         P(:,:,sig)= psig(sig)*P(:,:,sig);
% %         Q(:,:,sig)= psig(sig)*Tsig(:,:,sig)- Cest*P(:,:,sig)*Cest';
% %         K1(:,:,sig)= (sqrt(psig(sig))*Bsig(:,:,sig)-Asig(:,:,sig)* P(:,:,sig)*Cest')*inv(Q(:,:,sig));
% %     end
% % end

Knew = K;
end

