function [Aest,Kest,Qest,Pest,Cest,Best,H,Test]= stoch_lpv_realization_test(yt,pt,L,niter,psig)

% The function estimates system matrices A,K,C and 
% covariance matrices Q, P for LPV-SSA system:
% x(k+1) = A(p)x(k) + K(p)v(k); y = Cx(k)

% p is d-dimensional scheduling variable
% A(p) = A1*p_1 + ... + A_d*p_d   Affine 
% K(p) = K1*p_1 + ... + K_d*p_d   Affine

% Noise co-variances: for all sig=1,2,..d  
% P_{sig} = E{ x(k)(x(k))'(p_sig)^2 } 
% Q_{sig} = E{ v(k)(v(k))'(p_sig)^2 }

% All equations and definitions from:
% M. Petreczky and R. Vidal ``Realization Theory for a Class of Stochastic
% Bilinear Systems", IEEE Trans on Automatic Control, vol 63, Jan 2018. 
% The function implements Algorithm 2 Page 75

% @C Manas Mejari, 
% Created MM Feb 2018, Ecole Centrale Lille, University of Lille

% yt: Traning output, pt: Traning scheduling, 
% L: Estimate of state dimension, niter: Number of iterations for ARE
% psig = E{ (p_{sig})^2 }
  
%word length max (2*L-1)
% ## From words of length at most L (2L-1)
[d,M]= size(pt);
[ny,~]= size(yt);

W = cell((L-1)+1+1,1);  % Collection of words of lenth at most (L)
W{1}= [];
ind=1;
WW{ind} = [];       % WW is the set \Sigma^{*} 
ind= ind+1;
for l=1:(L-1)+1
    W{l+1} = permn((1:d)',l);
    
    for j=1:size(W{l+1})
        WW{ind} = W{l+1}(j,:);
        ind= ind+1;
    end
end


%% Step 1: Construct finite Hankel matrix H_{L,L+1}

H = compute_hankel_matrix_selection(yt,pt,L,WW,psig);


%% Step 2: Compute SVD decompositon of H = O*R

[U,S,V]= svd(H,'econ');
O = U*sqrt(S);
R = sqrt(S)*V';

[nm,~]= size(R);
%% Step 3: C estimated: First ny rows of O

Cest = O(1:ny,:);

%% Step 4: Partition of R 
ML1 = (d^((L-1)+2)-1)/(d-1); ML = (d^((L-1)+1)-1)/(d-1);

j=1;
CV = zeros(nm,ny*d,ML1);
for i=1:ML1
    CV(:,:,i) = R(:,j:j+ny*d-1);
    j = j+ny*d;
end

%% Step 5: Construct Bsig
Cv1 = CV(:,:,1);%Cv1 = R(:,1:ny*d);

j=1;
Bsig = zeros(nm,ny,d);
for i=1:d
    Bsig(:,:,i) = Cv1(:,j:j+ny-1);
    j=j+ny;
end

%% Step 6: Estimate Asig
Asig = zeros(nm,nm,d);
for sig=1:d
    CLHS=[];CRHS=[];
for i=1:ML
    
    vj = [WW{i} sig];   % vj = vi*sigma
    isSix = cellfun (@ (x) isequal (x, vj), WW);
    [~, indx] = find (isSix);
    
    % Solve X*A = B  for X in LS sense
    CLHS = [CLHS CV(:,:,i)];
    CRHS = [CRHS CV(:,:,indx)];
   
end
% LS Solution
CRHS = (1/sqrt(psig(sig)))*CRHS;

Asig(:,:,sig) = (CRHS/CLHS);

end

%% Step 7: Estimate K, Q, P, Riccati equation

% ## Assume Admisiible L is SIGMA^{+} take sum over all sigma in computing
% P step 7.3

% initialize P,Q,K
for sig=1:d
    P(:,:,sig) = zeros(nm,nm);
    Tsig(:,:,sig) =  compute_T(yt,pt,sig,psig,M);
    Q(:,:,sig) = psig(sig)*Tsig(:,:,sig) ;
    K(:,:,sig) = sqrt(psig(sig))*Bsig(:,:,sig)*inv(Q(:,:,sig));
end
Pold = P; Pnew = P;
Qold = Q; Qnew = Q;
Kold = K; Knew = K;

for i=1:niter

    
    for sig=1:d
         
        temp = zeros(nm,nm);
        for sig1=1:d
            % if (sig1,sig) in L (language)
%             P(:,:,sig) = P(:,:,sig)+ ...
%                          Asig(:,:,sig1)*P(:,:,sig1)*(Asig(:,:,sig1))'+...
%                          K(:,:,sig1)*Q(:,:,sig1)*(K(:,:,sig1))';

         temp = temp +...
                Asig(:,:,sig1)*Pold(:,:,sig1)*(Asig(:,:,sig1))'+...
                         Kold(:,:,sig1)*Qold(:,:,sig1)*(Kold(:,:,sig1))';
           
            
        end
        %P(:,:,sig)= psig(sig)*P(:,:,sig);
        Pnew(:,:,sig) =  psig(sig)*temp ;  % update P for each sigma
        Qnew(:,:,sig)=   psig(sig)*Tsig(:,:,sig)- Cest*Pnew(:,:,sig)*Cest';
        Knew(:,:,sig)= (sqrt(psig(sig))*Bsig(:,:,sig)-Asig(:,:,sig)* Pnew(:,:,sig)*Cest')*inv(Qnew(:,:,sig));
    
        norm(Pnew(:,:,sig)-Pold(:,:,sig))
    end
    
    Pold = Pnew;
    Qold = Qnew;
    Kold = Knew;
end


    





Aest= Asig; Best=Bsig;Pest=Pold; Qest=Qold; Kest=Kold; Test = Tsig;
end

