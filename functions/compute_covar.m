function Lam = compute_covar(yt,pt,w,psig,M)
% function compute_covar computes covariance Lam (eq (9))
%  M. Petreczky and R. Vidal ``Realization Theory for a Class of Stochastic
% Bilinear Systems", IEEE Trans on Automatic Control, vol 63, Jan 2018. 
% The function implements Algorithm 2 Page 75

% @C Manas Mejari, 
% Created MM Feb 2018, Ecole Centrale Lille, University of Lille

% w: word stored as row vector (sequnces of indices)
% Lam : E(y(t)z_{w}(t)') = (1/M) *sum(t=len(w)+1)^(M) y(t)z_w(t)' % Eq (6)

[ny,~] = size(yt);
    pw = 1; temp =zeros(ny,ny);
    
    for ind = 1:length(w)
        pw = pw*psig(w(ind));
        
    end
    
for t= length(w)+1:M
    
        zw=1;
        for ind = 1:length(w)
        zw = zw*pt(w(ind), t - length(w)+ind-1); %Eq (4), u_{w}(t-1)
        
        end
       
        zwt = (1/sqrt(pw))*yt(:,t - length(w))*zw; % Eq (6)

        
        temp = temp + yt(:,t)*(zwt)';
end

Lam = (1/M)*temp;

end

