function Psi = compute_covar_seq(yt,pt,w,psig,M)


% function compute_covar computes covariance Lam (eq (9))
% w: word stored as row vector (sequnces of indices)
% Psi(w) : [Lam_{1w} Lam{2w} ... Lam{dw}]

% Eq (9) and (10) of  M. Petreczky and R. Vidal ``Realization Theory for a Class of Stochastic
% Bilinear Systems", IEEE Trans on Automatic Control, vol 63, Jan 2018. 


% @C Manas Mejari, 
% Created MM Feb 2018, Ecole Centrale Lille, University of Lille


[d,~]= size(pt);

Psi = [];

if w==0
    w=[];
end

for ind=1:d
    
    tempP = compute_covar(yt,pt,[ind,w],psig,M);
    Psi = [Psi tempP]; 
    
end




end

