function [Hab,Habk,Hak,Hkb,Ad,Bd,Cd,Dd]= deter_lpv_realization_nice_Selection(yt,pt,ut,psig,alpha,beta,r)

% The function estimates system matrices A(p),B(p),C,D  
% covariance matrices Q, P for LPV-SSA system:
% x(k+1) = A(p)x(k-) + B(p)u(k) + K(p)v(k); y = Cx(k)+Du(k)+ v(k)

% p is d-dimensional scheduling variable
% A(p) = A1*p_1 + ... + A_d*p_d   Affine 
% B(p) = B1*p_1 + ... + B_d*p_d   Affine

% The function implements ``nice selection"
% selecting 'r*r' submatrix of Hankel Matrix


% All equations and definitions from:
% Pepijn B. Cox, Roland Toth andMihaly Petreczky "Estimation of LPV-SS Models with Static
% Dependency using Correlation Analysis" 1st IFAC Workshop on Linear Parameter Varying Systems
% Grenoble, France, October 7-9, 2015 

% @C Manas Mejari, 
% Created MM March 2019, Ecole Centrale Lille, University of Lille

% yt: Traning output, pt: Traning scheduling, 
% 
% psig = E{ (p_{sig})^2 }
  

% From words of length at most L+1 (2L+1)
[d,M]= size(pt);
[ny,~]= size(yt);
[nu,~]= size(ut);
Su = var(ut);



%% (r,n) - selection
% alpha \subset \Sigma^{n} prod {1,..ny}
% beta  \subset \Sigma^{n} prod {1,...,ny*d}
% cardinality(alpha) = cardinality(beta) = r

% randomly select 'r' word-index pairs
%rstop = 10; iter =0; % maxiter

Hab = zeros(r,r);




% Compute Hankel with this selection

for j=1:r
    for i=1:r
        w = [beta(j).wordB beta(j).word alpha(i).word alpha(i).wordC]
        disp('alpha')
        alpha(i)
        disp('word w')
        w
        disp('i,j')
        i
        j
        Psi_t = compute_covar_seq_deterministic(yt,pt,ut,w,psig,M);
        
        Hab(i,j) = (Psi_t(alpha(i).index,beta(j).index))/(Su(beta(j).index));
    end
end



%% Step 1: Form Z, Algo 1, Arxive

Habk = zeros(r,r,d);
for sig=1:d
    
    % form Z 
    
    for j=1:r
    for i=1:r
        w = [beta(j).wordB beta(j).word sig alpha(i).word alpha(i).wordC];
        Psi_t = compute_covar_seq_deterministic(yt,pt,ut,w,psig,M);
        
        Habk(i,j,sig) = (Psi_t(alpha(i).index,beta(j).index))/(Su(beta(j).index));
    end
    end
    
    
    
    %Asig(:,:,sig) = (1/sqrt(psig(sig)))*(Z(:,:,sig)/H_red);
    %sc = norm(Asig(:,:,sig));
    %sc = sqrt(psig(sig))*sqrt(d)*norm(Asig(:,:,sig))+1;
    %Asig(:,:,sig) = Asig(:,:,sig)/sc ;
end

%% Hak
Hak = zeros(r,nu,d);
for sig=1:d
        
    for j=1:nu
        for i=1:r
        w = [[] sig alpha(i).word alpha(i).wordC];
        Psi_t = compute_covar_seq_deterministic(yt,pt,ut,w,psig,M);
        
        Hak(i,j,sig) = (Psi_t(alpha(i).index,j))/(Su(j));  
        end
    end
end

%% Hkb (for C)
Hkb = zeros(ny,r);

for j=1:r
 for sig=1:d       
        for i=1:ny
        w = [beta(j).wordB beta(j).word  sig];
        Psi_t = compute_covar_seq_deterministic(yt,pt,ut,w,psig,M);
        %Psi_temp = Psi_temp/norm(Psi_temp);
        Hkb(i,j) = (Psi_t(i,beta(j).index))/(Su(beta(j).index));  
      end
  end    
end


%% Estimate matrices A,B,C,D

Ad= zeros(r,r,d); Bd= zeros(r,nu,d); Cd= zeros(ny,r); Dd = zeros(ny,nu);

for j=1:nu
        
        for i=1:ny
        w = [];
        Psi_t = compute_covar_seq_deterministic(yt,pt,ut,w,psig,M);
        %Psi_temp = Psi_temp/norm(Psi_temp);
        Dd(i,j) = (Psi_t(i,j))/(Su(j));  
        end
end

%   [U,S, V] = svd(Hab,0);         % Full rank decomposition

    %U = U(:,1:r);                % Select only the nx basis of the Hankel matrix
    %V = V(:,1:r);
    %S = S(1:r,1:r);

    % inverse reachability and observablity matrix.
    %Oinv = (S^(-1/2))*U';
    %Rinv = V * (S^(-1/2));
  
  Oinv = inv(Hab)
    Rinv = eye(size(Oinv))
    for i = 1:d
        Ad(:,:,i) = Oinv* Habk(:,:,i) * Rinv;

        % Discrete time
        eigtmp = eig(Ad(:,:,i));
        if(~isempty(find(  real(eigtmp).^2 + imag(eigtmp).^2 > 1  )))
            warning(['The matrix A',sprintf('%.0f',i),' is unstable. Poles outside unit disc']);
        end

        Bd(:,:,i) = Oinv* Hak(:,:,i);

        

    end
    Cd = Hkb*Rinv;

end



