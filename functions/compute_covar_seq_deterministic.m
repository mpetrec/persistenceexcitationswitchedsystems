function Psi = compute_covar_seq_deterministic(yt,pt,ut,w,psig,M)


% function compute_covar computes covariance Lam (eq (9))
% w: word stored as row vector (sequnces of indices)


% Cox et. al.IFAC eq(6)
% C A_{sigma_k}...A_{sigma_2} B_{sigma_{1}} = 
% E{ y(t) p_{sigma_k}(t-1)....p_{sigma_1}(t-k) u(t-k)}


% @C Manas Mejari, 
% Created MM March 2019, Ecole Centrale Lille, University of Lille


[d,~]= size(pt);
[ny,~] = size(yt);
[nu,~] = size(ut);

Psi = [];

if w==0
    w=[];
end

% for ind=1:d
%     
%     tempP = compute_covar(yt,pt,[ind,w],psig,M);
%     Psi = [Psi tempP]; 
%     
% end


    pw = 1; temp =zeros(ny,nu);
    
    for ind = 1:length(w)
        pw = pw*psig(w(ind));
        
    end
    
 
 if length(w)==0
    Psi = yt*ut';
    return;
 end
    
%for t= length(w)+1:M
     
 zw=pt(w(1),1:end-length(w)+1);
 for ind = 2:length(w)
                zw = zw.*pt(w(ind), ind:end-length(w)+ind); %Eq (4), u_{w}(t-1)
        
 end
       
  zwu = (1/sqrt(pw))*ut(:,1:end - length(w)+1).*zw; % Eq (6)

        
%        temp = temp + (1/sqrt(pw))*yt(:,t)*(zwu)';
%end

Psi = 1/(sqrt(pw))*yt(:,length(w):end)*zwu'/M;


end

