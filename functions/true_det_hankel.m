function [Hab_true] = true_det_hankel(C,A,B,base,psig)
% Calculates True hankel matrix

n = size(base.R,1);
d = length(psig);
[nx,~]= size(A(:,:,1));
Hab_true = zeros(n,n);
for i = 1:size(base.O,1)
        for j = 1:size(base.R,1)
            
            w = [base.R{j,1}, base.O{i,1}];
            Aw = eye(nx,nx); pw=1;
       if isempty(w)
           Aw = eye(nx,nx);
       else 
           for k=1:length(w)
               indx = w(k);
               
           Aw = A(:,:,indx+1)*Aw;
           
           end
       end

%Hab_true(i,j) = (C(:,:,base.O{j,2}+1)*Aw*B(:,:,base.R{j,2}+1));
Hab_true(i,j) = (C(base.O{j,3},:)*Aw*B(:,base.R{j,3},base.R{j,2}+1));
            
            
            
        end
end





end

