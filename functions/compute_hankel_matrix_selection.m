function H = compute_hankel_matrix_selection(yt,pt,L,WW,psig)
% function compute Hankel matrix defined in Eq (12)
% of M. Petreczky and R. Vidal ``Realization Theory for a Class of Stochastic
% Bilinear Systems", IEEE Trans on Automatic Control, vol 63, Jan 2018. 
% Step 1 of Algorithm 2 Page 75

% @C Manas Mejari, 
% Created MM Feb 2018, Ecole Centrale Lille, University of Lille

% WW is the set defined by \Sigma^{*}
% set of all possible words with an empty element

[d,M]= size(pt);  % d: dimension of scheduling, M: Length of traning data

MLr =  (d^((L-1)+1)-1)/(d-1);
MLc =  (d^((L-1)+2)-1)/(d-1);
Psi_c=[];
for i=1:MLr
    Psi_r=[];
    for j=1:MLc
       w =  [WW{j} WW{i}];
       
       if isempty(w)
           w=0;
       end
       
       Psi_temp = compute_covar_seq(yt,pt,w,psig,M);
       Psi_r= [Psi_r Psi_temp];
        
    end
    Psi_c = [Psi_c;Psi_r];
end

H = Psi_c;



end

