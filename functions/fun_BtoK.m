function K = fun_BtoK(B,A,C,psig,niter)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
% # dimension of B(:,:,sig) is nx*ny
if (isempty(B))
    B(:,:,1) = [0;0;1];
    B(:,:,2) = [0;0;1];
end

%% Check stability



[nm,~,d]= size(A);
[ny,~]= size(C);
Bsig = B; Asig=A; Cest=C;

Atil= zeros(size(kron(A(:,:,1),A(:,:,1))));
for i=1:d
  Atil =  Atil+ psig(i)*kron(A(:,:,i),A(:,:,i));
end
eig(Atil)

for sig=1:d
Q(:,:,sig) = psig(sig)*eye(ny,ny);  % for noise variance ==1

end
% initialize P,Q,K
for sig=1:d
    P(:,:,sig) = zeros(nm,nm);
%    Tsig(:,:,sig) =  compute_T(yt,pt,sig,psig,M);
%    Q(:,:,sig) = psig(sig)*Tsig(:,:,sig) ;
    K(:,:,sig) = sqrt(psig(sig))*Bsig(:,:,sig)*inv(Q(:,:,sig));
end
Pold = P; Pnew = P;
%Qold = Q; Qnew = Q;
Kold = K; Knew = K;

for i=1:niter

    
    for sig=1:d
         
        temp = zeros(nm,nm);
        for sig1=1:d
            

         temp = temp +...
                Asig(:,:,sig1)*Pold(:,:,sig1)*(Asig(:,:,sig1))'+...
                         Kold(:,:,sig1)*Q(:,:,sig1)*(Kold(:,:,sig1))';
           
            
        end
        %P(:,:,sig)= psig(sig)*P(:,:,sig);
        Pnew(:,:,sig) =  psig(sig)*temp ;  % update P for each sigma
   %     Qnew(:,:,sig)=   psig(sig)*Tsig(:,:,sig)- Cest*Pnew(:,:,sig)*Cest';
        Knew(:,:,sig)= (sqrt(psig(sig))*Bsig(:,:,sig)-Asig(:,:,sig)*Pnew(:,:,sig)*Cest')*inv(Q(:,:,sig));
    
       % norm(Pnew(:,:,sig)-Pold(:,:,sig))
    end
    
    Pold = Pnew;
 %   Qold = Qnew;
    Kold = Knew;
end

K= Kold;
end

