function T = compute_T(yt,pt,sig,psig,M)

% Function computes covariance eq (9)
% M. Petreczky and R. Vidal ``Realization Theory for a Class of Stochastic
% Bilinear Systems", IEEE Trans on Automatic Control, vol 63, Jan 2018. 



[ny,~] = size(yt);
     temp = zeros(ny,ny);
     pw = psig(sig);
        
  
    
for t= length(sig)+1:M
    
        
        zw = pt(sig, t - length(sig));
        zwt = (1/sqrt(pw))*(yt(:,t - length(sig))*zw);
        temp = temp + zwt*(zwt)';
end

T = (1/M)*temp;

end

