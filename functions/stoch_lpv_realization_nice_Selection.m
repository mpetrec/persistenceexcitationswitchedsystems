function [Aest,Kest,Qest,Pest,Cest,Best,H_red,H1,Test,Z]= stoch_lpv_realization_nice_Selection(yt,pt,L,niter,psig,r,n,alpha,beta)

% The function estimates system matrices A,K,C and 
% covariance matrices Q, P for LPV-SSA system:
% x(k+1) = A(p)x(k) + K(p)v(k); y = Cx(k)

% p is d-dimensional scheduling variable
% A(p) = A1*p_1 + ... + A_d*p_d   Affine 
% K(p) = K1*p_1 + ... + K_d*p_d   Affine

% The function implements ``nice selection"
% selecting 'r*r' submatrix of Hankel Matrix

% Noise co-variances: for all sig=1,2,..d  
% P_{sig} = E{ x(k)(x(k))'(p_sig)^2 } 
% Q_{sig} = E{ v(k)(v(k))'(p_sig)^2 }

% All equations and definitions from:
% M. Petreczky and R. Vidal ``Realization Theory for a Class of Stochastic
% Bilinear Systems", IEEE Trans on Automatic Control, vol 63, Jan 2018. 
% The function implements Algorithm 2 Page 75

% @C Manas Mejari, 
% Created MM Feb 2018, Ecole Centrale Lille, University of Lille

% yt: Traning output, pt: Traning scheduling, 
% L: Estimate of state dimension, niter: Number of iterations for ARE
% psig = E{ (p_{sig})^2 }
  

% From words of length at most L+1 (2L+1)
[d,M]= size(pt);
[ny,~]= size(yt);

% W = cell(L+1,1);  % Collection of words of lenth at most (L)
% W{1}= [];
% ind=1;
% WW{ind} = [];          % WW is the set \Sigma^{*} 
% W_selection{ind} = []; % W_selection is the set \Sigma^{n} of max word length n
% ind= ind+1;
% for l=1:L
%     W{l+1} = permn((1:d)',l);
%     
%     for j=1:size(W{l+1})
%         WW{ind} = W{l+1}(j,:);
%         if (l<=2*n-1)
%             W_selection{ind} = W{l+1}(j,:);
%         end
%         ind= ind+1;
%     end
% end

H1=[];
%H1 = compute_hankel_matrix_selection(yt,pt,L,WW,psig);
%% (r,n) - selection
% alpha \subset \Sigma^{n} prod {1,..ny}
% beta  \subset \Sigma^{n} prod {1,...,ny*d}
% cardinality(alpha) = cardinality(beta) = r

% randomly select 'r' word-index pairs
%rstop = 10; iter =0; % maxiter

H_red = zeros(r,r);
%$stop_c=1;

%while (stop_c)
  
 

%aw = randperm(length(W_selection)); 
%bw = randperm(length(W_selection));

% for i=1:r
%     alpha(i).word= W_selection{aw(i)};
%     alpha(i).index = randi(ny);
%     
%     beta(i).word= W_selection{bw(i)};
%     beta(i).index = randi(ny*d);
%     
% end

% Compute Hankel with this selection

for j=1:r
    for i=1:r
        w = [beta(j).word alpha(i).word];
        Psi_t = compute_covar_seq(yt,pt,w,psig,M);
        
        H_red(i,j) = Psi_t(alpha(i).index,beta(j).index);
    end
end
%iter = iter +1;
%if iter < rstop
%stop_c = (rank(H1) ~= rank(H_red));
%else
%    stop_c = 0;
%end

%end

%if (rank(H1) ~= rank(H_red))
%    warning('Rank condition of Hankel matrix not satisfied');
%end

%H1 = H1/norm(H1);
%H_red = H_red/norm(H_red);


%% Step 1: Form Z, Algo 1, Arxive
Z = zeros(r,r,d);
for sig=1:d
    
    % form Z 
    
    for j=1:r
    for i=1:r
        w = [[beta(j).word sig] alpha(i).word];
        Psi_temp = compute_covar_seq(yt,pt,w,psig,M);
        
        Z(i,j,sig) = Psi_temp(alpha(i).index,beta(j).index);
    end
    end
    
    % constranint optimization
    %options = sdpsettings('solver', 'gurobi','verbose',0,'cachesolvers',1);
%     yalmip('clear');
%     Ah = sdpvar(r,r,'full');
%     %Constraints = norm(Ah,'fro') <= (1/(sqrt(psig(sig))*sqrt(d)))- eps;
%     %Objective = 1*norm(Ah*H_red*(sqrt(psig(sig))) -Z(:,:,sig),'fro');
%     
%     Constraints = [];
%     
%     Constraints = Ah*H_red == Z(:,:,sig)/sqrt(psig(sig));
%     %Constraints = Constraints + [norm(Ah,'fro') <= (1/(sqrt(psig(sig))*sqrt(d)))- eps]; 
%     %Objective =   norm(((1/(sqrt(psig(sig))*sqrt(d)))-eps)- norm(Ah,'fro'));
%     Objective = [];
%     diagnostics = optimize(Constraints,Objective);
%     if diagnostics.problem == 0
%        disp('Solver thinks it is feasible')
%     elseif diagnostics.problem == 1
%        disp('Solver thinks it is infeasible')
%     else
%        disp('Something else happened')
%     end
%   
%     Asig(:,:,sig) = value(Ah);
    
    Asig(:,:,sig) = (1/sqrt(psig(sig)))*(Z(:,:,sig)/H_red);
    %sc = norm(Asig(:,:,sig));
    %sc = sqrt(psig(sig))*sqrt(d)*norm(Asig(:,:,sig))+1;
    %Asig(:,:,sig) = Asig(:,:,sig)/sc ;
end

%rank(H1)
rank(H_red)


%% Step 2: Form B
for i=1:r
row_index(i) = alpha(i).index;
end
for j=1:ny*d
       
        %H_temp = H1(:,j); % columns index by (\eps,j)
        
        
        for i=1:r
        w = [[] alpha(i).word];
        Psi_temp = compute_covar_seq(yt,pt,w,psig,M);
        %Psi_temp = Psi_temp/norm(Psi_temp);
        B(i,j) = Psi_temp(row_index(i),j);  
        end
end
j=1;
for sig=1:d
    
    Bsig(:,:,sig) = B(:,j:j+ny-1);
    j = j + ny;
    
end
%% Step 3: Form Cest
for i=1:r
col_index(i) = beta(i).index;
end
for j=1:r
       
        %H_temp = H1(i,:); % row indexed by (\eps,i)
        
        %Cest(i,:) = H_temp(col_index);
        
        for i=1:ny
        w = [beta(j).word, []];
        Psi_temp = compute_covar_seq(yt,pt,w,psig,M);
        %Psi_temp = Psi_temp/norm(Psi_temp);
        Cest(i,j) = Psi_temp(i,col_index(j));  
        end
end

Cest = Cest*inv(H_red);
nm = r;

%% Step 7: Estimate K, Q, P, Riccati equation

% ## Assume Admisiible L is SIGMA^{+} take sum over all sigma in computing
% P step 7.3

% initialize P,Q,K
for sig=1:d
    P(:,:,sig) = zeros(nm,nm);
    Tsig(:,:,sig) =  compute_T(yt,pt,sig,psig,M);
    Q(:,:,sig) = psig(sig)*Tsig(:,:,sig) ;
    K(:,:,sig) = sqrt(psig(sig))*Bsig(:,:,sig)*inv(Q(:,:,sig));
end
Pold = P; Pnew = P;
Qold = Q; Qnew = Q;
Kold = K; Knew = K;

for i=1:niter

    
    for sig=1:d
         
        temp = zeros(nm,nm);
        for sig1=1:d
            % if (sig1,sig) in L (language)


         temp = temp +...
                Asig(:,:,sig1)*Pold(:,:,sig1)*(Asig(:,:,sig1))'+...
                         Kold(:,:,sig1)*Qold(:,:,sig1)*(Kold(:,:,sig1))';
           
            
        end
        
        Pnew(:,:,sig) =  psig(sig)*temp ;  % update P for each sigma
        Qnew(:,:,sig)=   psig(sig)*Tsig(:,:,sig)- Cest*Pnew(:,:,sig)*Cest';
        Knew(:,:,sig)= (sqrt(psig(sig))*Bsig(:,:,sig)-Asig(:,:,sig)* Pnew(:,:,sig)*Cest')*inv(Qnew(:,:,sig));
    
        %norm(Pnew(:,:,sig)-Pold(:,:,sig))
        norm(Qnew(:,:,sig)-Qold(:,:,sig))
    end
    
    Pold = Pnew;
    Qold = Qnew;
    Kold = Knew;
end

    





Aest= Asig; Best=Bsig;Pest=Pold; Qest=Qold; Kest=Kold; Test = Tsig;
end

