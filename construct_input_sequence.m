function [u,p,t_index]=construct_input_sequence(word,input_ind,dict_inv,np,nu,ny)
	new_word=[]
	for j=1:size(word,2)
		new_word=[dict_inv{word(j)+1}-1,new_word];
	end 
	q=word(1);

	%new_word

	u=zeros(nu,size(word,2)*2+1)
	p=zeros(np,size(word,2)*2+1)

	u(input_ind,1)=1;
	for i=1:size(word,2)
		p(word(i)+1,i)=1;
	end
	for i=size(word,2)+1:size(word,2)*2
		p(new_word(i-size(word,2))+1,i)=1;
	end
	p(q+1,size(word,2)*2+1)=1;
	u(input_ind,size(word,2)*2+1)=-1;
	t_index=size(word,2);
end		
	
