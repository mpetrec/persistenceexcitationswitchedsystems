%generate s_i diamon s_i^{-1}
function [u,p,t_index]=construct_input_sequenceSystem(word,input_ind,np,nu,ny,A,B,C,contr_base)
	xfinal=B(:,input_ind,word(1)+1);

	for i=2:size(word,2)
		xfinal = A(:,:,word(i)+1)*xfinal;
	end

	nx=size(A,1);

	[uc,new_word,cmatrix]=generate_control_input(xfinal,A,B,C,nx,np,nu,ny,contr_base);

	[xf2,x,y]=simulateSystem(A,B,C,np,nx,nu,ny,uc,new_word,xfinal);

	disp('Final state1')
	xf2

	%new_word

	u=zeros(nu,size(word,2)+size(new_word,2));
	p=zeros(np,size(word,2)+size(new_word,2));

	u(input_ind,1)=1;
	for i=1:size(word,2)
		p(word(i)+1,i)=1;
	end
	for i=size(word,2)+1:size(p,2)
		p(new_word(i-size(word,2))+1,i)=1;
		u(:,i)=uc(:,i-size(word,2));
	end
	t_index=size(word,2);
	disp('Size p')
	size(p)
	disp('Size uc')	
	size(uc)

	[xf2,x,y]=simulateSystem(A,B,C,np,nx,nu,ny,u,[word,new_word],zeros(nx,1));
	disp('Final state')
	x(:,size(x,2)-1:size(x,2))
	y(t_index)
	disp('xfinal')
	xfinal
	disp('xf')
	x(:,t_index+1)

end		
	
